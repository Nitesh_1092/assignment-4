import PyPDF2
import textract
import regex as re
import string
import simplejson
from rake_nltk import Rake
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
ps = PorterStemmer()
file = 'JavaBasics-notes.pdf'
pdfFileObj = open(file,'rb')
pdfReader = PyPDF2.PdfFileReader(pdfFileObj)

num_pages = pdfReader.numPages
count = 0
text = ""

while count < num_pages:
    pageObj = pdfReader.getPage(count)
    count += 1
    text += pageObj.extractText()
    
    
if text != "":
    text = text
else:
    text = textract.process(fileurl, method = 'tesseract', language = 'eng')
    
no_digits = []
for i in text:
    if not i.isdigit():
        no_digits.append(i)
        
result = ''.join(no_digits)

for k in result.split("\n"):
    final = re.sub(r"[^a-zA-Z0-9]+",' ',k)
str=[]
   
for k in final.split(" "): 
    if len(k) != 1 and len(k) != 2 and len(k) !=3:  
        str.append(k)
        str.append(" ")

final1 = ''.join(str)
                    
r = Rake()
r.extract_keywords_from_text(final1)

j = r.get_ranked_phrases_with_scores()

output = open('Rake-NltkSol.txt', 'w')
simplejson.dump(j,output)
output.close()





import PyPDF2
import textract
import string
import regex as re
import nltk
from nltk.tokenize import word_tokenize
from nltk.collocations import *
import simplejson
file = 'JavaBasics-notes.pdf'
pdfFileObj = open(file,'rb')
pdfReader = PyPDF2.PdfFileReader(pdfFileObj)             ###Reading the pdf file

num_pages = pdfReader.numPages
count = 0
text = ""

while count < num_pages:
    pageObj = pdfReader.getPage(count)
    count += 1
    text += pageObj.extractText()
    
    
if text != "":
    text = text                                          ## Extracting text using textract
else:
    text = textract.process(fileurl, method = 'tesseract', language = 'eng')
 
no_digits = []

for i in text:                                       ##Removing Digits
    if not i.isdigit():
        no_digits.append(i)
        
result = ''.join(no_digits)

for k in result.split("\n"):                             ##Removing special characters and punctuations
    final = re.sub(r"[^a-zA-Z0-9]+",' ',k)
##print(final)    
str=[]    
for k in final.split(" "):                            ## Removing words of one or two lengths
    if len(k) != 1 and len(k) != 2:  
        str.append(k)
        str.append(" ")

final1 = ''.join(str) 
bigram_measures = nltk.collocations.BigramAssocMeasures()        
trigram_measures = nltk.collocations.TrigramAssocMeasures()
tokens = word_tokenize(final1)                                ## Tokenizing my text into words


finder = BigramCollocationFinder.from_words(tokens)       ## Finding bigram and trigram for the text
finder1 = TrigramCollocationFinder.from_words(tokens)

finder.apply_freq_filter(3)
finder1.apply_freq_filter(3)

str1 = []
str2 = []                                                 ##printing bigram with their pmi values 
print("bigram Keywords with pmi weights")
for i in finder.score_ngrams(bigram_measures.pmi):
    str1.append(i)
    
print("\n")
print("Trigram Keywords with pmi weights")                ## printing trigram with their pmi values
for i in finder1.score_ngrams(trigram_measures.pmi):
    str2.append(i)
    
output = open('bigram_solution.txt', 'w')
simplejson.dump(str1,output)
output.close()

output = open('Trigram_solution.txt', 'w')
simplejson.dump(str2,output)
output.close()



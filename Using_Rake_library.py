import PyPDF2
import textract
import regex as re
import RAKE.RAKE as rake
import operator
import simplejson
ps = PorterStemmer()
file = 'JavaBasics-notes.pdf'              ##Read the pdf file
pdfFileObj = open(file,'rb')
pdfReader = PyPDF2.PdfFileReader(pdfFileObj)

num_pages = pdfReader.numPages
count = 0
text = ""

while count < num_pages:                          ## Extract text using textract
    pageObj = pdfReader.getPage(count)
    count += 1
    text += pageObj.extractText()
    
    
if text != "":
    text = text
else:
    text = textract.process(fileurl, method = 'tesseract', language = 'eng')
    
no_digits = []
for i in text:                       ## Removing digits 
    if not i.isdigit():
        no_digits.append(i)
        
result = ''.join(no_digits)

for k in result.split("\n"):            ## Removing punctations and special words
    final = re.sub(r"[^a-zA-Z0-9]+",' ',k)
str=[]    
   
for k in final.split(" "):              ## Removing words having one, two or three characters as they are of less significance
    if len(k) != 1 and len(k) != 2 and len(k) !=3:  
        str.append(k)
        str.append(" ")

final1 = ''.join(str)

rake_object = rake.Rake("SmartStoplist.txt") ## Using RAKE library to extract keywords
keyword = rake_object.run(final1)

output = open('Using_Rake_library.txt', 'w')   ## Writing output in RakeSolution.txt
simplejson.dump(keyword,output)
output.close()
